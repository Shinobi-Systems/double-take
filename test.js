const apiHost = 'http://localhost:3000'
const doubleTake = require('./index.js') // require('node-double-take')
const apiHost = process.argv[2]
const password = process.argv[3]

if(!apiHost || !password){
    console.log(`Missing API Host and/or Password`)
    console.log(`node test.js "http://localhost:3000" "thePassword"`)
}

const {
    authPost,
    authTokens,
    cameraCamera,
    config,
    filesystemFolders,
    logger,
    match,
    recognize,
    recognizeTest,
    recognizeUploadPost,
    apiRequest,
    uploadFile,
} = doubleTake(apiHost);

async function runTest(){
    const authResponse = await authPost({
        password: password
    })
    console.log(authResponse)
}
runTest().then(() => {
    console.log(`Done test!`)
})
