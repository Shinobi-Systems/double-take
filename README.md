# Double Take wrapper in Node.js

Unofficial Node.js Wrapper for Double Take Facial Recognition by Shinobi Systems (https://shinobi.video)

All HTTP API Requests seen in the Double Take Documentation should work with this wrapper.

For each available function you can find their possible parameters in the Double Take documentation.

Double Take documentation : https://documenter.getpostman.com/view/1013188/TzsWuAa8#intro

Initialize Wrapper

```
const apiHost = 'http://localhost:3000'
const doubleTake = require('node-double-take')

const {
    authPost,
    authPasswordPost,
    authPasswordPatch,
    authTokens,
    authTokensPost,
    authTokensDelete,
    cameraCamera,
    config,
    configPatch,
    configSecrets,
    configSecretsPatch,
    configTheme,
    configThemePatch,
    filesystemFolders,
    filesystemFoldersNamePost,
    filesystemFoldersNameDelete,
    logger,
    loggerDelete,
    match,
    matchDelete,
    proxy,
    recognize,
    recognizeTest,
    recognizeUploadPost,
    statusConfig,
    storageLatestFilename,
    storageMatchesFilename,
    storageTrainDelete,
    storageTrainNameFilename,
    train,
    trainIdPatch,
    trainAddNamePost,
    trainRemoveNameDelete,
    trainRetrainName,
    trainStatus,
    apiRequest,
    uploadFile,
} = doubleTake(apiHost);
```

Simple API Calls with `apiRequest`. First Parameter is the target, Second are the options.

Example of `/auth`

```
const authResponse = await authPost({
    password: 'password',
})
```

Example of `/recognize/upload` with file buffer.

```
const fileBuffer = fs.readFileSync(filePath);
const file = {
   fileData: fileBuffer,
   fileName: 'example.jpg',
}
recognizeUploadPost(file).then((data) => {
    console.log(data)
})
```

Example of `/recognize/upload` with createReadStream.

```
const file = {
   fileData: fs.createReadStream(filePath),
   fileName: 'example.jpg',
}
recognizeUploadPost(file).then((data) => {
    console.log(data)
})
```

Example of `/config/theme`

```
const themePatchResponse = await configThemePatch({
    "ui": "bootstrap4-dark-blue",
    "editor": "nord_dark"
})
```
